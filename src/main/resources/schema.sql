create table recipe (
    id integer generated always as identity,
    name varchar(255) not null,
    servings varchar(255) not null,
    tags text[] not null,
    ingredients text not null,
    steps text not null,
    image_url varchar(255)
);

create table users (
    email varchar primary key,
    salt varchar not null,
    hashed_password varchar not null,
    status varchar not null,
    role varchar not null,
    validation_token varchar
);