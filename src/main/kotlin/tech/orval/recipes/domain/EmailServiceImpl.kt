package tech.orval.recipes.domain

import jakarta.mail.*
import jakarta.mail.internet.InternetAddress
import jakarta.mail.internet.MimeBodyPart
import jakarta.mail.internet.MimeMessage
import jakarta.mail.internet.MimeMultipart
import tech.orval.recipes.domain.api.EmailService
import java.util.*

class EmailServiceImpl: EmailService {

    override fun send(to: String, subject: String, content: String) {
        val prop = Properties()
        prop["mail.smtp.auth"] = true
        prop["mail.smtp.ssl.enable"] = true
        prop["mail.smtp.starttls.enable"] = "true"
        prop["mail.smtp.host"] = System.getProperty("SMTP_HOST")
        prop["mail.smtp.port"] = System.getProperty("SMTP_PORT")
        prop["mail.smtp.ssl.trust"] = System.getProperty("SMTP_HOST")

        val session = Session.getInstance(prop, object : Authenticator() {
            override fun getPasswordAuthentication(): PasswordAuthentication {
                return PasswordAuthentication(System.getProperty("SMTP_EMAIL"), System.getProperty("SMTP_PASSWORD"))
            }
        })

        val message = MimeMessage(session)
        message.setFrom(InternetAddress(System.getProperty("EMAIL_SENDER")))
        message.setRecipients(
            Message.RecipientType.TO, InternetAddress.parse(to)
        )
        message.subject = subject

        val mimeBodyPart = MimeBodyPart()
        mimeBodyPart.setContent(content, "text/html; charset=utf-8")

        val multipart: Multipart = MimeMultipart()
        multipart.addBodyPart(mimeBodyPart)

        message.setContent(multipart)

        Transport.send(message)
    }

}