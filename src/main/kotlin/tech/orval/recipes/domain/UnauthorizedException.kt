package tech.orval.recipes.domain

class UnauthorizedException(message: String): Exception(message)