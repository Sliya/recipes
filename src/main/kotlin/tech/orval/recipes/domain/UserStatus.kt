package tech.orval.recipes.domain

enum class UserStatus {
    ACTIVE, INACTIVE
}