package tech.orval.recipes.domain

import tech.orval.recipes.domain.api.RecipeService
import tech.orval.recipes.domain.spi.RecipeRepository

class RecipeServiceImpl(private val recipeRepository: RecipeRepository) : RecipeService {

    override fun listRecipes(): List<Recipe> {
        return recipeRepository.listRecipes()
    }

    override fun getRecipe(id: Int): Recipe? {
        return recipeRepository.getRecipe(id)
    }

    override fun create(recipe: Recipe, user: User): Recipe {
        if (!user.isAdmin) throw UnauthorizedException("Il faut être connecté pour créer une recette")
        return recipeRepository.create(recipe)
    }

    override fun deleteById(id: Int, user: User) {
        if (!user.isAdmin) throw UnauthorizedException("Il faut être connecté pour supprimer une recette")
        recipeRepository.deleteById(id)
    }

    override fun save(recipe: Recipe, user: User) {
        if (!user.isAdmin) throw UnauthorizedException("Il faut être connecté pour enregistrer une recette")
        recipeRepository.save(recipe)
    }
}