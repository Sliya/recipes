package tech.orval.recipes.domain.api

import tech.orval.recipes.domain.Role
import tech.orval.recipes.domain.User

interface UserService {
    fun createUser(name: String, role: Role, password: String)
    fun validate(email: String, token: String)
    fun get(email: String): User?
    fun auth(email: String, password: String): User?
    fun deny(email: String, token: String)
}