package tech.orval.recipes.domain.api

import tech.orval.recipes.domain.Recipe
import tech.orval.recipes.domain.User

interface RecipeService {
    fun listRecipes(): List<Recipe>
    fun getRecipe(id: Int): Recipe?
    fun create(recipe: Recipe, user:User): Recipe
    fun deleteById(id: Int, user: User)
    fun save(recipe: Recipe, user: User)
}