package tech.orval.recipes.domain.api

interface EmailService {

    fun send(to: String, subject: String, message: String)
}