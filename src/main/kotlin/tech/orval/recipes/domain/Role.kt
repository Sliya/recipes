package tech.orval.recipes.domain

enum class Role {
    OPEN, ADMIN
}