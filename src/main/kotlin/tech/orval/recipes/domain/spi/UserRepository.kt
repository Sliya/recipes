package tech.orval.recipes.domain.spi

import tech.orval.recipes.domain.User

interface UserRepository {
    fun create(user:User)
    fun get(email: String): User?
    fun save(user: User): User
    fun delete(email: String)
}