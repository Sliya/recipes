package tech.orval.recipes.domain.spi

import tech.orval.recipes.domain.Recipe

interface RecipeRepository {
    fun listRecipes(): List<Recipe>
    fun getRecipe(id: Int): Recipe?
    fun create(recipe: Recipe): Recipe
    fun deleteById(id: Int)
    fun save(recipe: Recipe)
}