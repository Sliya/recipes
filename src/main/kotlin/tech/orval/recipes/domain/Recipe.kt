package tech.orval.recipes.domain

import java.net.URL


data class Recipe(
    val id: Int?,
    val name: String,
    val servings: String,
    val tags: List<String>,
    val ingredients: String,
    val steps: String,
    val imageURL: URL?
)
