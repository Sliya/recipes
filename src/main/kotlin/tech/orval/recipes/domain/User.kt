package tech.orval.recipes.domain

data class User(
    val email: String,
    val status: UserStatus,
    val role: Role,
    val salt: String,
    val hashedPassword: String,
    val validationToken: String
) {

    companion object {
        val ANONYMOUS = User("anonymous", UserStatus.ACTIVE, Role.OPEN, "", "", "")
    }

    val isAdmin = role == Role.ADMIN && status == UserStatus.ACTIVE
}
