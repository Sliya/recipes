package tech.orval.recipes.domain

import tech.orval.recipes.domain.api.EmailService
import tech.orval.recipes.domain.api.UserService
import tech.orval.recipes.domain.spi.UserRepository
import java.security.SecureRandom
import java.security.spec.KeySpec
import java.util.*
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec


class UserServiceImpl(private val userRepository: UserRepository, private val emailService: EmailService): UserService {
    override fun createUser(name: String, role: Role, password: String) {
        val salt = generateSalt()
        val user = User(name, UserStatus.INACTIVE, role, saltToString(salt), hash(password, salt), UUID.randomUUID().toString())
        userRepository.create(user)
        sendVerificationEmail(user.email, user.validationToken)
    }

    override fun validate(email: String, token: String) {
        val user = userRepository.get(email) ?: throw IllegalArgumentException("User not found")
        if (user.validationToken != token) {
            throw IllegalArgumentException("Invalid token")
        }
        userRepository.save(user.copy(status = UserStatus.ACTIVE))
        sendConfirmationEmail(user.email)
    }

    private fun sendConfirmationEmail(email: String) {
        emailService.send(email, "Compte utilisateur validé", "Votre compte utilisateur a été validé")
    }

    override fun deny(email: String, token: String) {
        val user = userRepository.get(email) ?: throw IllegalArgumentException("User not found")
        if (user.validationToken != token) {
            throw IllegalArgumentException("Invalid token")
        }
        userRepository.delete(email)
        sendDenyEmail(user.email)
    }

    private fun sendDenyEmail(email: String) {
        emailService.send(email, "Compte utilisateur refusé", "Votre compte utilisateur a été refusé")
    }

    override fun get(email: String): User? {
        return userRepository.get(email)
    }

    override fun auth(email: String, password: String): User? {
        val user = userRepository.get(email) ?: return null
        if (user.status == UserStatus.INACTIVE) return null
        val hash = hash(password, stringToSalt(user.salt))
        return if (hash == user.hashedPassword) user else null
    }

    private fun generateSalt(): ByteArray {
        val random = SecureRandom()
        val salt = ByteArray(16)
        random.nextBytes(salt)
        return salt
    }

    private fun saltToString(salt: ByteArray): String {
        val saltString = salt.contentToString()
        return saltString.substring(1,saltString.length-1)
    }

    private fun stringToSalt(saltString: String): ByteArray {
        return saltString.split(", ").map{it.toByte()}.toByteArray()
    }

    private fun hash(password: String, salt: ByteArray): String {
        val spec: KeySpec = PBEKeySpec(password.toCharArray(), salt, 65536, 128)
        val factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
        return factory.generateSecret(spec).encoded.joinToString("") { "%02x".format(it) }
    }

    private fun sendVerificationEmail(email: String, validationToken: String) {
        emailService.send(System.getProperty("VERIFICATION_EMAIL"),
            "Validation compte utilisateur recettes.orval.tech",
            "Un nouvel utilisateur a été créé. Lien de validation <br>" +
                    "<a href='${System.getProperty("BASE_URL")}/validate/$email/$validationToken'>Valider</a><br>" +
                    "<a href='${System.getProperty("BASE_URL")}/deny/$email/$validationToken'>Refuse</a>")
    }

}