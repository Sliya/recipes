package tech.orval.recipes.infrastruture.controller

import io.javalin.Javalin

interface Controller {

    fun registerRoutes(app: Javalin)
}