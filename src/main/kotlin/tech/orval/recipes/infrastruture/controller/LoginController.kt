package tech.orval.recipes.infrastruture.controller

import io.javalin.Javalin
import io.javalin.http.Context
import io.javalin.http.Handler
import tech.orval.recipes.domain.Role
import tech.orval.recipes.domain.User
import tech.orval.recipes.domain.UserStatus
import tech.orval.recipes.domain.api.UserService
import java.security.SecureRandom

class LoginController(private val userService: UserService): Controller {

    companion object {
        val LOGIN_FORM_ROUTE = "/loginform"
    }

    override fun registerRoutes(app: Javalin) {
        app.get(LOGIN_FORM_ROUTE, serveLoginPage)
        app.get("/signupform", serveSignupForm)
        app.get("/logout", logout)
        app.post("/login", login)
        app.post("/signup", signup)
        app.get("/validate/{email}/{token}", validate)
        app.get("/deny/{email}/{token}", deny)
    }

    private val logout = Handler { ctx -> ctx.sessionAttribute("user", null); ctx.redirect("/") }

    private val serveLoginPage = Handler { ctx: Context ->
        ctx.render("loginform.kte")
    }

    private val serveSignupForm = Handler { ctx: Context ->
        ctx.render("signupform.kte")
    }

    private val login = Handler { ctx ->
        val email = ctx.formParam("email") ?: throw IllegalArgumentException("Email is required")
        val password = ctx.formParam("password") ?: throw IllegalArgumentException("Password is required")
        val user = userService.auth(email, password)
        if (user == null) {
            ctx.render("loginform.kte", mapOf("error" to true))
            return@Handler
        }
        ctx.sessionAttribute("user", user)
        ctx.redirect("/")
    }

    private val signup = Handler { ctx ->
        val email = ctx.formParam("email") ?: throw IllegalArgumentException("Email is required")
        val password = ctx.formParam("password") ?: throw IllegalArgumentException("Password is required")
        userService.createUser(email, Role.ADMIN, password)
        ctx.render("signupform.kte", mapOf("success" to true))
    }

    private val validate = Handler { ctx ->
        val email = ctx.pathParam("email")
        val token = ctx.pathParam("token")
        userService.validate(email, token)
        ctx.redirect("/loginform")
    }

    private val deny = Handler { ctx ->
        val email = ctx.pathParam("email")
        val token = ctx.pathParam("token")
        userService.deny(email, token)
        ctx.redirect("/")
    }
}