package tech.orval.recipes.infrastruture.controller

import io.javalin.Javalin
import io.javalin.http.Context
import io.javalin.http.Handler
import io.javalin.util.FileUtil
import tech.orval.recipes.domain.Recipe
import tech.orval.recipes.domain.UnauthorizedException
import tech.orval.recipes.domain.User
import tech.orval.recipes.domain.api.RecipeService
import java.lang.ProcessBuilder.Redirect
import java.net.URI
import java.util.*

class RecipeController(private val recipeService: RecipeService) : Controller {

    private val serveList = Handler { ctx ->
        ctx.render(
            "main.kte",
            mapOf(
                "recipes" to recipeService.listRecipes(),
                "user" to ctx.sessionAttribute<User?>("user")
            )
        )
    }

    private val serveRecipe = Handler { ctx ->
        val id = ctx.pathParam("id").toInt()
        ctx.render(
            "recipe.kte",
            mapOf(
                "recipe" to recipeService.getRecipe(id),
                "user" to ctx.sessionAttribute<User?>("user")
            )
        )
    }

    private val serveAddForm = Handler { ctx ->
        val user = ctx.sessionAttribute<User>("user")
            ?: throw UnauthorizedException("Il faut être connecté pour ajouter une recette")
        ctx.render("addRecipe.kte", mapOf("user" to user))
    }

    private val serveEditForm = Handler { ctx ->
        val user = ctx.sessionAttribute<User>("user")
            ?: throw UnauthorizedException("Il faut être connecté pour éditer une recette")
        val id = ctx.pathParam("id").toInt()
        val recipe = recipeService.getRecipe(id) ?: throw IllegalArgumentException("Recette non trouvée")
        ctx.render("addRecipe.kte", mapOf("recipe" to recipe, "user" to user))
    }

    private val handleRecipeAdd = Handler { ctx ->
        val user = extractUser(ctx)
        val recipeToSave = extractRecipe(ctx)
        recipeService.create(recipeToSave, user)
        ctx.redirect("/")
    }

    private val handleRecipeEdit = Handler { ctx ->
        val user = extractUser(ctx)
        val recipeToSave = extractRecipe(ctx)
        if (recipeToSave.id == null) throw IllegalArgumentException("L'id de la recette est requis")
        val oldRecipe =
            recipeService.getRecipe(recipeToSave.id) ?: throw IllegalArgumentException("Recette non trouvée")
        if (oldRecipe.imageURL != null) {
            deleteImageOfRecipe(oldRecipe)
        }
        recipeService.save(recipeToSave, user)
        ctx.redirect("/")
    }

    private fun extractRecipe(ctx: Context): Recipe {
        val id = ctx.formParam("id")?.toInt()
        val name = ctx.formParam("name") ?: throw IllegalArgumentException("Le nom de la recette est requis")
        val steps = ctx.formParam("steps") ?: throw IllegalArgumentException("Les étapes sont requises")
        val servings = ctx.formParam("servings") ?: throw IllegalArgumentException("Les quantités sont requises")
        val ingredients = ctx.formParam("ingredients") ?: throw IllegalArgumentException("Les ingrédients sont requis")
        val tags = ctx.formParam("tags") ?: throw IllegalArgumentException("Les tags sont requis")
        val file = ctx.uploadedFile("image")
        val imageUrl = if (file!!.size() > 0) {
            val uuid = UUID.randomUUID()
            val tmpPath = "/tmp/$uuid"
            val imagePath = "${System.getProperty("UPLOAD_DIR")}/" + uuid + ".webp"
            FileUtil.streamToFile(file.content(), tmpPath)
            "magick convert $tmpPath -resize 256x256 -quality 80 $imagePath".runCommand()
            "rm $tmpPath".runCommand()
            URI.create("${System.getProperty("BASE_URL")}/$uuid.webp").toURL()
        } else {
            null
        }
        return Recipe(id, name, servings, tags.split(";"), ingredients, steps, imageUrl)
    }

    private val handleDelete = Handler { ctx ->
        val user = extractUser(ctx)
        val id = ctx.pathParam("id").toInt()
        val recipe = recipeService.getRecipe(id) ?: return@Handler
        deleteImageOfRecipe(recipe)
        recipeService.deleteById(id, user)
    }

    private fun deleteImageOfRecipe(recipe: Recipe) {
        val imageName = recipe.imageURL.toString().split('/').last()
        "rm ${System.getProperty("UPLOAD_DIR")}/$imageName".runCommand()
    }

    private fun extractUser(ctx: Context): User {
        return ctx.sessionAttribute<User?>("user") ?: User.ANONYMOUS
    }

    private fun String.runCommand() {
        ProcessBuilder(*split(" ").toTypedArray())
            .redirectOutput(Redirect.INHERIT)
            .redirectError(Redirect.INHERIT)
            .start()
            .waitFor()
    }

    override fun registerRoutes(app: Javalin) {
        app.get("/", serveList)
        app.get("/recipe/add", serveAddForm)
        app.post("/recipe/add", handleRecipeAdd)
        app.post("/recipe/edit", handleRecipeEdit)
        app.get("/recipe/edit/{id}", serveEditForm)
        app.get("/recipe/{id}", serveRecipe)
        app.delete("/recipe/{id}", handleDelete)
    }
}