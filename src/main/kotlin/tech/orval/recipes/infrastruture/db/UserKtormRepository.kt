package tech.orval.recipes.infrastruture.db

import org.ktorm.database.Database
import org.ktorm.dsl.*
import tech.orval.recipes.domain.User
import tech.orval.recipes.domain.spi.UserRepository

class UserKtormRepository(private val database: Database): UserRepository {
    override fun create(user: User) {
        database.insert(Users) {
            set(it.email, user.email)
            set(it.status, user.status)
            set(it.salt, user.salt)
            set(it.hashedPassword, user.hashedPassword)
            set(it.role, user.role)
            set(it.validationToken, user.validationToken)
        }
    }

    override fun get(email: String): User? {
        return database.from(Users)
            .select()
            .where { Users.email eq  email }
            .map { row ->
                User(
                    email = row[Users.email]!!,
                    status = row[Users.status]!!,
                    role = row[Users.role]!!,
                    salt = row[Users.salt]!!,
                    hashedPassword = row[Users.hashedPassword]!!,
                    validationToken = row[Users.validationToken]!!
                )
            }.firstOrNull()
    }

    override fun save(user: User): User {
        database.update(Users) {
            set(it.status, user.status)
            set(it.salt, user.salt)
            set(it.hashedPassword, user.hashedPassword)
            set(it.role, user.role)
            set(it.validationToken, user.validationToken)
            where { it.email eq user.email }
        }
        return user
    }

    override fun delete(email: String) {
        database.delete(Users) {
            it.email eq email
        }
    }
}