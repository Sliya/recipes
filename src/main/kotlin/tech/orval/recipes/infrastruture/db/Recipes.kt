package tech.orval.recipes.infrastruture.db

import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.text
import org.ktorm.schema.varchar
import org.ktorm.support.postgresql.textArray

object Recipes : Table<Nothing>("recipe"){

    val id = int("id").primaryKey()
    val name = varchar("name")
    val servings = varchar("servings")
    val tags = textArray("tags")
    val ingredients = text("ingredients")
    val steps =  text("steps")
    val imageURL = varchar("image_url")
}