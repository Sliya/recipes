package tech.orval.recipes.infrastruture.db

import org.ktorm.database.Database
import org.ktorm.dsl.*
import tech.orval.recipes.domain.Recipe
import tech.orval.recipes.domain.spi.RecipeRepository
import java.net.URI

class RecipeKtormRepository(private val database: Database) : RecipeRepository {

    override fun listRecipes(): List<Recipe> =
        database
            .from(Recipes)
            .select()
            .map { rowToRecipe(it) }
            .toList()

    override fun getRecipe(id: Int): Recipe? =
        database
            .from(Recipes)
            .select()
            .where(Recipes.id eq id)
            .map {
                rowToRecipe(it)
            }.firstOrNull()

    override fun create(recipe: Recipe): Recipe {
        assert(recipe.id == null)
        val id = database.insertAndGenerateKey(Recipes) {
            set(it.name, recipe.name)
            set(it.steps, recipe.steps)
            set(it.ingredients, recipe.ingredients)
            set(it.servings, recipe.servings)
            set(it.tags, recipe.tags.toTypedArray())
            set(it.imageURL, recipe.imageURL?.toString())
        } as Int

        return recipe.copy(id=id)
    }

    override fun deleteById(id: Int) {
        database.delete(Recipes){it.id eq id}
    }

    override fun save(recipe: Recipe) {
        assert(recipe.id != null)
        database.update(Recipes){
            set(it.name, recipe.name)
            set(it.steps, recipe.steps)
            set(it.ingredients, recipe.ingredients)
            set(it.servings, recipe.servings)
            set(it.tags, recipe.tags.toTypedArray())
            if(recipe.imageURL != null) {
                set(it.imageURL, recipe.imageURL.toString())
            }
            where { it.id eq recipe.id!! }
        }
    }

    private fun rowToRecipe(row: QueryRowSet): Recipe {
        return Recipe(id = row[Recipes.id]!!,
            name = row[Recipes.name]!!,
            servings = row[Recipes.servings]!!,
            tags = row[Recipes.tags]!!.toList().filterNotNull(),
            ingredients = row[Recipes.ingredients]!!,
            steps = row[Recipes.steps]!!,
            imageURL = row[Recipes.imageURL]?.let { URI.create(it).toURL() })
    }
}