package tech.orval.recipes.infrastruture.db

import org.ktorm.schema.Table
import org.ktorm.schema.enum
import org.ktorm.schema.varchar
import tech.orval.recipes.domain.Role
import tech.orval.recipes.domain.UserStatus

object Users: Table<Nothing>("users"){
    val email = varchar("email").primaryKey()
    val salt = varchar("salt")
    val hashedPassword = varchar("hashed_password")
    val status = enum<UserStatus>("status")
    val role = enum<Role>("role")
    val validationToken = varchar("validation_token")
}