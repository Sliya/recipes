package tech.orval.recipes

import io.javalin.Javalin
import io.javalin.http.staticfiles.Location
import io.javalin.rendering.template.JavalinJte
import org.ktorm.database.Database
import tech.orval.recipes.domain.EmailServiceImpl
import tech.orval.recipes.domain.RecipeServiceImpl
import tech.orval.recipes.domain.UnauthorizedException
import tech.orval.recipes.domain.UserServiceImpl
import tech.orval.recipes.infrastruture.controller.LoginController
import tech.orval.recipes.infrastruture.controller.RecipeController
import tech.orval.recipes.infrastruture.db.RecipeKtormRepository
import tech.orval.recipes.infrastruture.db.UserKtormRepository

fun main() {
    val db = Database.connect(
        System.getProperty("DATABASE_URL"),
        user = System.getProperty("DATABASE_USERNAME"),
        password = System.getProperty("DATABASE_PASSWORD"))
    val recipeRepository = RecipeKtormRepository(db)
    val recipeService = RecipeServiceImpl(recipeRepository)
    val userRepository = UserKtormRepository(db)
    val emailService = EmailServiceImpl()
    val userService = UserServiceImpl(userRepository, emailService)

    val app = Javalin.create{
        it.staticFiles.add(System.getProperty("UPLOAD_DIR"), Location.EXTERNAL)
        it.staticFiles.add("/public")
        it.fileRenderer(JavalinJte())
    }.start(System.getProperty("PORT").toInt())

    RecipeController(recipeService).registerRoutes(app)
    LoginController(userService).registerRoutes(app)

    app.exception(UnauthorizedException::class.java) { _, ctx ->
        ctx.redirect(LoginController.LOGIN_FORM_ROUTE)
    }
}
