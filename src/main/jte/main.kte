@param recipes: List<tech.orval.recipes.domain.Recipe>
@param user: tech.orval.recipes.domain.User?

@template.layout(content=@`

<div class="container is-flex is-justify-content-space-around is-align-content-start is-flex-wrap-wrap	">
    @for(recipe in recipes)
        <div class="card recipe" id="recipe-${recipe.id}">
            <div class="card-image">
                <figure class="image is-square">
                    <img
                            src="${recipe.imageURL?.toString() ?: "/fallback.webp"}"
                            alt="${recipe.name} image"
                    />
                </figure>
            </div>
            <div class="card-content">
                <div class="content">
                    <h4 class="title is-4"><a href="/recipe/${recipe.id}" class="has-text-inherit">${recipe.name}</a></h4>
                </div>
                <div class="tags">
                    @for(tag in recipe.tags)
                    <span class="tag">${tag}</span>
                    @endfor
                </div>
            </div>

            @if(user != null)
            <footer class="card-footer">
                <a href="/recipe/edit/${recipe.id}" class="card-footer-item">Edit</a>
                <a hx-delete="/recipe/${recipe.id}" hx-swap="delete" hx-target="#recipe-${recipe.id}" class="card-footer-item has-text-danger">Delete</a>
            </footer>
            @endif
        </div>
    @endfor
</div>

`, user=user)