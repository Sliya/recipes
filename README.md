# Recettes

This is just a collection of vegan recipes. It is build with [Kotlin](https://kotlinlang.org/) and [Javalin](https://javalin.io/)

## Build

Run `mvn clean install`

## Configuration

The following properties are required :
* `BASE_URL` : the base URL of the server;
* `DATABASE_URL` : the URL of the database;
* `DATABASE_USER` : the user of the database;
* `DATABASE_PASSWORD` : the password of the database;
* `UPLOAD_DIR` : the directory where the uploaded files will be stored;
* `PORT` : the port on which the server will listen;
* `SMTP_EMAIL` : the email address of the SMTP server;
* `SMTP_PASSWORD` : the password of the SMTP server;
* `EMAIL_SENDER` : the email address of the sender;
* `SMTP_HOST` : the host of the SMTP server;
* `SMTP_PORT` : the port of the SMTP server;
* `VERIFICATION_EMAIL` : the email address to send verification links.

## Run

mvn exec:java -Dexec.mainClass=tech.orval.recipes.AppKt -Dxxx=yyy